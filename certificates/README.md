# Certificates

## Synopsys

Permet d'ajouter des certificats dans la liste des autorités de confiances dans le système (/usr/local/share/ca-certificates).

## Variables

| nom | type | defaut | description | 
|---|---|---|---|
| ast_certificates | complex multivalué | AstCertificatesType | Liste de paires "domain" / "certificate" |

## Exemples

    # my_inventory.yml
    all:
      vars:
        ast_certificates:
          - domain: obsolete.domain.com
            state: absent
          
          - domain: my.domain.com
            content: |
                -----BEGIN CERTIFICATE-----
                MIIFPDCCBCSgAwIBAgISA6GJyko1XTsjD3d7eLPe+RB0MA0GCSqGSIb3DQEBCwUA
                MDIxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MQswCQYDVQQD
                EwJSMzAeFw0yMzEwMDIwMzM5MDZaFw0yMzEyMzEwMzM5MDVaMCIxIDAeBgNVBAMT
                F2pzb25zY2hlbWF2YWxpZGF0b3IubmV0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8A
                MIIBCgKCAQEAlTHxdo2LCh/pkP8TsiJZrAG6bnbmW+kUwbnR41xYIKmOxuNWjRKr
                ytcQhMlBJTprPv3YzMXZbCvIU+5zSHRmk30S1tr/aGsWXu/pSKo+uJU494pTEOOD
                y11m0NjAn90rul90IvDsEhc1//YabSPhM1jfGhrSc/z0N9cJFpOl3Ltv3nJdDNWh
                XKjwruJeQePSDYK3UqjZsvSAwi26jCd8QFdYwmqyclnX78aQzM8/h8e/eZshRgIX
                q+QIhJk3N5tO2SttobGmQrExferoS1ePeQE2Uk88BsE1jSGYEh7k5DWx9v4Uo1Hi
                ehrlqe68KjlIhGEJylNzDgpEB0A2vnpFXwIDAQABo4ICWjCCAlYwDgYDVR0PAQH/
                BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAMBgNVHRMBAf8E
                AjAAMB0GA1UdDgQWBBSjPfLvX1O/LXZPhv9Sko7IpseMBTAfBgNVHSMEGDAWgBQU
                LrMXt1hWy65QCUDmH6+dixTCxjBVBggrBgEFBQcBAQRJMEcwIQYIKwYBBQUHMAGG
                FWh0dHA6Ly9yMy5vLmxlbmNyLm9yZzAiBggrBgEFBQcwAoYWaHR0cDovL3IzLmku
                bGVuY3Iub3JnLzBjBgNVHREEXDBaghdqc29uc2NoZW1hdmFsaWRhdG9yLm5ldIIO
                bmV3dG9uc29mdC5jb22CG3d3dy5qc29uc2NoZW1hdmFsaWRhdG9yLm5ldIISd3d3
                Lm5ld3RvbnNvZnQuY29tMBMGA1UdIAQMMAowCAYGZ4EMAQIBMIIBBAYKKwYBBAHW
                eQIEAgSB9QSB8gDwAHYAejKMVNi3LbYg6jjgUh7phBZwMhOFTTvSK8E6V6NS61IA
                AAGK7q7HHAAABAMARzBFAiBbGZ7zIhPrEVLKDzbUvtHh2YzJVNohAJ7++5Ki8nes
                hgIhALLmkrawoqFfeqNMs4yIXJ9qQdd7Fmj9AW6zcPQ5cjv4AHYAtz77JN+cTbp1
                8jnFulj0bF38Qs96nzXEnh0JgSXttJkAAAGK7q7HFwAABAMARzBFAiA/1l2YQhQ1
                +FdHYI/5F4SnX9j6wOOfG2GeEeigmilq/QIhAJhzMo93WzghWFfxsQxaV/ZEXRXV
                8MOl8ZdH1S53c2zTMA0GCSqGSIb3DQEBCwUAA4IBAQAOe9kyAAt9+tT1VW1vgqc5
                vrQC3bLnPtqJF7PZA/pp0loCxBTXUrGp7aOXcS87D4GOTS+NowOkvhn76+YrNB8k
                FCGzLzOFCbNPN9lYYTpuS5xoyEHtLFIP3u3zWSY94wlX0cuJe4vfGaIuQqTVg3e1
                wMrDcyOX0Z4DRAgPgdXWMgOrvq318eU1sty9y43RF6/mRYQ+xHs6Gl09HZ+/JNUU
                WNX5vnzRQ8NY8V50jbNm7N1af05Y5j0lYsT8iGlQ1ulgjpJW07+7TDba/Co8ff6S
                17WMMs4MdPdJMstf+6MOfKvtz92lYM972vZEwJ6JIhV5o0a3rYJvLRSeX61LB1yk
                -----END CERTIFICATE-----

    # exec
    ansible-playbook -i my_inventory.yml autostack/certificates/playbook.yml


## Définitions

### AstHostifyDomainsType

    {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "array",
        "default": [],
        "items": [
            {
                "type": "object",
                "properties": {
                    "domain": {
                        "type": "string"
                    },
                    "state": {
                        "type": "string",
                        "enum": ["absent", "present"],
                        "default": "present"
                    },
                    "content": {
                        "type": "string"
                    }
                },
                "required": [
                    "domain",
                    "content"
                ]
            }
        ]
    }