# Hostify

## Synopsys

Permet de personnaliser le fichier /etc/hosts en fonction de votre inventaire.

## Variables

| nom | type | defaut | description | 
|---|---|---|---|
| ast_hostify_domains | complex multivalué | AstHostifyDomainsType | Liste de paires "ip" / "name" |

## Exemples

    # my_inventory.yml
    all:
      vars:
        ast_hostify_domains:
          - ip: 1.2.3.4
            name: my.domain.com
          - ip: 1.2.3.1
            name: other.domain.com
            state: absent
    
    # exec
    ansible-playbook -i my_inventory.yml autostack/hotify/playbook.yml


## Définitions

### AstHostifyDomainsType

    {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "array",
        "default": [],
        "items": [
            {
                "type": "object",
                "properties": {
                    "ip": {
                        "type": "string"
                    },
                    "name": {
                        "type": "string"
                    },
                    "state": {
                        "type": "string",
                        "enum": ["absent", "present"],
                        "default": "present"
                    }
                },
                "required": [
                    "ip",
                    "name"
                ]
            }
        ]
    }